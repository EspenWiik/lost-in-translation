import { Link, useHistory } from 'react-router-dom'
import { fetchTranslation, deleteTranslations } from './ProfileAPI'
import { useState, useEffect } from 'react'
const Profile = (props) => {

    let history = useHistory()
    const logedIn = props.logedIn

    if (logedIn !== true) {
        history.push('/')
    }

    const [fire, setFire] = useState([])



    useEffect(() => {


        const renderTranslations = async () => {
            try {
                let translations = await fetchTranslation()
                let translationsArr = translations.map(a => a.input);
                let lastTenTranslation = translationsArr.slice(Math.max(translationsArr.length - 10, 0))
                setFire(lastTenTranslation)

                return lastTenTranslation
            }
            catch (e) {
                console.log(e)
            }
        }
        renderTranslations()
    }, [])

    const onDeleteClicked = async () => {
        try {
            await deleteTranslations()
        }
        catch (e) {
            console.log(e);
        }

    }

    const onSignOutClicked = () => {
        props.onClicked(false)

        history.push('/')
    }

    return (
        <div>
            <h4>Profile Name</h4>
            <div className="translateBox">
                <h2>Last Translations:</h2>
                <ul>
                    {fire.map(char => (
                        <li>{char}</li>)
                    )}
                </ul>
            </div>
            <button type="button" onClick={onDeleteClicked}>
                Delete translations
                </button>
            <Link to="/">
                <button type="button" onClick={onSignOutClicked}>
                    Sign Out
                </button>
            </Link>
        </div>

    )
}
export default Profile