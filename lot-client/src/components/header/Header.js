import './Header.css'
import logo from '../../LostInTranslation_Resources/Logo.png'
import { Link } from 'react-router-dom'

const Header = () => {
    return (
        <header className="header">
            <Link to="/translator">
                <img className="logo" src={logo} alt="My logo" />
            </Link>
            <Link to="/translator">
                <h1 className="title">Lost in Translation</h1>
            </Link>
            <Link to="/profile">
                <button type="button">
                    Profile
                </button>
            </Link>
        </header>
    )
}

export default Header