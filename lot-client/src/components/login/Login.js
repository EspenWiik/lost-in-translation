import { useHistory } from 'react-router-dom'
import { useState } from "react";
import { checkUsername, createUsername } from './LoginAPI'
import { setStorage } from '../../utils/Storage';

const Login = (props) => {


    let history = useHistory()
    const [username, setUsername] = useState('')

    const onLoginClick = async () => {
        try {
            const foundUser = await checkUsername(username)
            if (foundUser) {

                setStorage('_user-ss', username)
                props.onClicked(true)
                history.push('/translator')
                return;
            }

            const createdUser = await createUsername(username)
            setStorage('_user-ss', username)
            console.log(createdUser);
            props.onClicked(true)
            history.push('/translator')
        }
        catch (e) {
            console.log(e);
        }
    }
    return (
        <container className="loginBox">
            <h2>Login to Translator</h2>
            <form>
                <input className="loginField"
                    type="text" placeholder="Type Name to Log in"
                    onChange={e => setUsername(e.target.value)} />

                <button type="button" className="loginBtn" onClick={onLoginClick}>Sign In</button>

            </form>
        </container>
    )
}
export default Login