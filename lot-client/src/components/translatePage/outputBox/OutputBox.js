import './OutputBox.css'


const OutputBox = (props) => {

    return (
        <div className="outputBox">
            {props.data.map((char) => (
                <img className="signImgs" src={require(`../../../LostInTranslation_Resources/individial_signs/${char}.png`).default} alt="bab" />
            ))}
        </div>
    )
}

export default OutputBox