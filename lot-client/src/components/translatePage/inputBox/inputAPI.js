const BASE_URL = "http://localhost:5000"

export function postTranslation(input) {

    return fetch(`${BASE_URL}/translations`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ input })
    })
        .then(r => r.json())
}