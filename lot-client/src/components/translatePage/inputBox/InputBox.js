import { useState } from 'react'
import { postTranslation } from './inputAPI'
import { setStorage } from '../../../utils/Storage'
import { useHistory } from 'react-router-dom'
import './InputBox.css'

const InputBox = (props) => {

    let history = useHistory()
    const logedIn = props.logedIn

    if (logedIn !== true) {
        history.push('/')
    }

    const [input, setInput] = useState('')
    const [inputArr, setInputArr] = useState([])


    const onInputChanged = event => {
        setInput(event.target.value)
    }

    const onTranslateClicked = async () => {
        try {
            const newTranslation = await postTranslation(input)
            setStorage('_translate-ss', input)
            console.log(newTranslation);
        }
        catch (e) {
            console.log(e);
        }
        let lowerInput = input.toLowerCase()
        let str2 = lowerInput.replaceAll(" ", "1")
        console.log(str2)
        let inputArr = str2.split('')
        console.log(inputArr);
        setInputArr(inputArr)
        props.clicked(inputArr)
    }

    return (
        <container className="inputBox">
            <input className="inputField"
                type="text" placeholder="Type in words to translate..."
                value={input} onChange={onInputChanged} />
            <button className="inputBtn" onClick={onTranslateClicked}>Translate</button>

        </container>
    )
}
export default InputBox