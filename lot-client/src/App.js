import Header from './components/header/Header'
import InputBox from './components/translatePage/inputBox/InputBox.js'
import OutputBox from './components/translatePage/outputBox/OutputBox.js'
import Login from './components/login/Login.js'
import Profile from './components/profile/Profile.js'
import { useEffect, useState } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { getStorage } from './utils/Storage'
//import ResultBox from './components/resultBox/ResultBox'



function App() {
  const [logedIn, setLogedIn] = useState('false')

  function handleLogInChange(newState) {
    setLogedIn(newState)

  }
  console.log('app rendering')

  useEffect(() => {
    const user = getStorage('_translate-ss')
    console.log(user);
  }, [])


  const [inputArr, setInputArr] = useState([])

  const handleTranslateClicked = (inputArr) => {
    console.log('clicked translate', inputArr)
    setInputArr(inputArr)
  }

  return (
    <BrowserRouter>
      <div className="container">
        <Header />

        <Switch>
          <Route path exact="/">
            <Login
              logedIn={logedIn} onClicked={handleLogInChange} />
          </Route>
          <Route path="/translator" >
            <InputBox logedIn={logedIn} clicked={handleTranslateClicked} />
            <OutputBox
              data={inputArr} />
          </Route>
          <Route path="/profile">
            <Profile logedIn={logedIn} onClicked={handleLogInChange} />

          </Route>

        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
