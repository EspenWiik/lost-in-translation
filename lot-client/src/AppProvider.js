// its a component
import { createContext, useState } from 'react'

export const AppContext = createContext()

export function AppProvider( props ) {

    const [ movies, setMovies ] = useState([ {id: 1, title: "best movie", description: "good"} ])

    return (
        <AppContext.Provider value={ [movies, setMovies] }>
            { props.children }
        </AppContext.Provider>
    )
}